package com.cgi.dentistapp.service.impl;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.entity.DentistVisitEntity;
import com.cgi.dentistapp.mapping.DentistMapper;
import com.cgi.dentistapp.mapping.DentistVisitMapper;
import com.cgi.dentistapp.repository.DentistVisitRepository;
import com.cgi.dentistapp.service.DataAcessException;
import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class DentistVisitServiceImpl implements DentistVisitService {

    private final DentistVisitRepository repository;
    private final DentistVisitMapper mapper;
    private final DentistMapper dentistMapper;

    @Autowired
    public DentistVisitServiceImpl(DentistVisitRepository repository,
                                   DentistVisitMapper mapper,
                                   DentistMapper dentistMapper) {
        this.repository = repository;
        this.mapper = mapper;
        this.dentistMapper = dentistMapper;
    }

    public void saveVisit(DentistVisitDTO visitDto) {
        DentistVisitEntity visitEntity = mapper.mapToEntity(visitDto);
        repository.save(visitEntity);
    }

    public List<DentistVisitDTO> getAll() {
        List<DentistVisitDTO> result =
                mapper.mapToListOfDto(repository.findAll());

        return result;
    }

    @Override
    public DentistVisitDTO getById(Long id) {
        DentistVisitDTO result =
                mapper.mapToDto(repository.findOne(id));

        return result;
    }

    public void removeVisit(Long visitId) throws DataAcessException {
        repository.delete(visitId);
    }

    @Override
    public DentistVisitDTO getByDentistIdAndDateTime(Long dentistId, Date visitDateTime) {

        DentistVisitEntity entity =
                repository.findByDentistIdAndVisitDateTime(dentistId, visitDateTime);

        return entity != null ?
                mapper.mapToDto(entity)
                : null;
    }

}
