package com.cgi.dentistapp.service.impl;

import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.mapping.DentistMapper;
import com.cgi.dentistapp.repository.DentistRepository;
import com.cgi.dentistapp.service.DataAcessException;
import com.cgi.dentistapp.service.DentistService;
import org.modelmapper.MappingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class DentistServiceImpl implements DentistService {

    private final DentistRepository repository;
    private final DentistMapper mapper;

    @Autowired
    public DentistServiceImpl(DentistRepository repository, DentistMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }


    public List<DentistDTO> getAll() {
            List<DentistEntity> entities = repository.findAll();
            return mapper.mapToListOfDto(entities);
    }


    public DentistDTO getById(Long id) throws DataAcessException{
            DentistEntity entity = repository.getOne(id);
            return mapper.mapToDto(entity);
    }
}

