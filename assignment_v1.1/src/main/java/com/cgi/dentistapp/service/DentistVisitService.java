package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.dto.DentistVisitDTO;

import java.util.Date;
import java.util.List;

public interface DentistVisitService {

    List<DentistVisitDTO> getAll();

    DentistVisitDTO getById(Long id);

    void saveVisit(DentistVisitDTO visit) throws DataAcessException;

    void removeVisit(Long visitId) throws DataAcessException;

    DentistVisitDTO getByDentistIdAndDateTime(Long dentistId, Date visitDateTime);

}
