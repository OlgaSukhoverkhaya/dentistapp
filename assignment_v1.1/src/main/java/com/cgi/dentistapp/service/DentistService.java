package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dto.DentistDTO;

import java.util.List;

public interface DentistService {
    List<DentistDTO> getAll();
    DentistDTO getById(Long id) throws DataAcessException;
}
