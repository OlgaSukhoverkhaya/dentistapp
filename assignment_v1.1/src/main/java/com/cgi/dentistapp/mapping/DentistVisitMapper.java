package com.cgi.dentistapp.mapping;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.entity.DentistVisitEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class DentistVisitMapper extends BaseMapper<DentistVisitEntity, DentistVisitDTO> {

    @Autowired
    public DentistVisitMapper() {
        super(DentistVisitEntity.class,
                DentistVisitDTO.class);
    }
}
