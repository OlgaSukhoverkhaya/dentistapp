package com.cgi.dentistapp.mapping;

import com.cgi.dentistapp.dto.BaseDTO;
import com.cgi.dentistapp.entity.BaseEntity;

import java.util.List;

public interface Mapper<E extends BaseEntity, D extends BaseDTO> {

    E mapToEntity(D dto);
    D mapToDto(E entity);

    List<E>  mapToListOfEntities(List<D> dtoList);
    List<D> mapToListOfDto(List<E> entityList);
}
