package com.cgi.dentistapp.mapping;

import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.entity.DentistEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DentistMapper extends BaseMapper<DentistEntity, DentistDTO> {

    @Autowired
    public DentistMapper() {
        super(DentistEntity.class, DentistDTO.class);
    }
}
