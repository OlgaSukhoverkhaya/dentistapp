package com.cgi.dentistapp.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public abstract class BaseDTO implements Serializable {

    public Long id;

    public BaseDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
