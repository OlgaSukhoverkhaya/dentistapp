package com.cgi.dentistapp.dto;


import javax.validation.constraints.Size;

public class DentistDTO extends BaseDTO {

    @Size(min = 1, max = 25)
    String firstname;

    @Size(min = 1, max = 25)
    String lastname;

    public DentistDTO() {
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
