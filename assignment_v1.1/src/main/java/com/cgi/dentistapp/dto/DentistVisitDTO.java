package com.cgi.dentistapp.dto;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;

import java.util.Date;

public class DentistVisitDTO extends BaseDTO {

    //Fields for template
    @NotBlank
    public String visitDateString;

    @NotBlank
    public String visitTimeString;

    @NotNull
    public Long dentistId;

    //Fields for mapping to entity
    public DentistDTO dentist;

    @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm")
    public Date visitDateTime;

    public DentistVisitDTO(DentistDTO dentist, Date visitTime) {
        this.dentist = dentist;
        this.visitDateTime = visitTime;
    }

    public DentistVisitDTO() {
    }

    public Long getDentistId() {
        return dentistId;
    }

    public void setDentistId(Long dentistId) {
        this.dentistId = dentistId;
    }

    public String getVisitDateString() {
        return visitDateString;
    }

    public String getVisitTimeString() {
        return visitTimeString;
    }

    public void setVisitDateString(String visitDateString) {
        this.visitDateString = visitDateString;
    }


    public void setVisitTimeString(String visitTimeString) {
        this.visitTimeString = visitTimeString;
    }

    public DentistDTO getDentist() {
        return dentist;
    }

    public void setDentist(DentistDTO dentist) {
        this.dentist = dentist;
    }

    public Date getVisitDateTime() {
        return visitDateTime;
    }

    public void setVisitDateTime(Date visitDateTime) {
        this.visitDateTime = visitDateTime;
    }
}
