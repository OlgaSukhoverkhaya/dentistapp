package com.cgi.dentistapp.repository;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.entity.DentistEntity;
import com.cgi.dentistapp.entity.DentistVisitEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface DentistVisitRepository extends JpaRepository<DentistVisitEntity, Long> {

    DentistVisitEntity findByDentistIdAndVisitDateTime(Long dentistId, Date visitDate);
}
