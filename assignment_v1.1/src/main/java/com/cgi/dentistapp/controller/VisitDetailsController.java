package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@EnableAutoConfiguration
public class VisitDetailsController {

    @Autowired
    DentistVisitService dentistVisitService;

    @RequestMapping(value = "/visitdetails/{id}", method = RequestMethod.GET)
    public String showVisitDetails(Model model, @PathVariable("id") Long visitId) {
        DentistVisitDTO visit = dentistVisitService.getById(visitId);
        model.addAttribute("visit", visit);

        return "/visitdetails";
    }

}
