package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.service.DataAcessException;
import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@EnableAutoConfiguration
public class ResultsController {

    @Autowired
    DentistVisitService dentistVisitService;

    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = "/results/delete/{id}", method = RequestMethod.DELETE)
    public String deleteRegistration(@PathVariable("id") Long visitId,
                                     Model model) {
        try {
            dentistVisitService.removeVisit(visitId);
            model.addAttribute("registrations", dentistVisitService.getAll());

        } catch (DataAcessException ex) {

            model.addAttribute("error",
                    messageSource.getMessage("visit.remove.error",
                            null, LocaleContextHolder.getLocale()));
        }

        return "redirect:/results";
    }


    @RequestMapping(value = "/results", method = RequestMethod.GET)
    public String showRegistrations(Model model, @ModelAttribute("isSuccess") Object flashAttr) {
        List<DentistVisitDTO> regitrstaions = dentistVisitService.getAll();

        if (flashAttr != null) {
            model.addAttribute("success", flashAttr);
        }

        if (regitrstaions.isEmpty()) {
            model.addAttribute("warning", messageSource
                    .getMessage("empty.registrations.warning", null, LocaleContextHolder.getLocale()));
        } else {
            model.addAttribute("registrations", regitrstaions);
        }

        return "results";
    }
}
