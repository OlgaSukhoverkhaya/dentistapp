package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.service.DentistService;
import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
@EnableAutoConfiguration
public class FormPageController {

    @Autowired
    private DentistVisitService dentistVisitService;

    @Autowired
    private DentistService dentistService;

    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = {"/", "/{id}"}, method = RequestMethod.GET)
    public String showRegisterForm(Model model,
                                   @PathVariable("id") Optional<Long> id) {

        List<DentistDTO> dentistList = dentistService.getAll();
        DentistVisitDTO visit = new DentistVisitDTO();

        if (id.isPresent()) {
            visit = dentistVisitService.getById(id.get());
        }

        model.addAttribute("dentists", dentistList);
        model.addAttribute("visit", visit);

        return "form";
    }

    @RequestMapping(value = {"/", "/{id}"}, method = {RequestMethod.POST, RequestMethod.PUT})
    public String postRegisterForm(@Valid @ModelAttribute("visit") DentistVisitDTO visit,
                                   BindingResult bindingResult,
                                   Model model,
                                   @PathVariable("id") Optional<Long> id,
                                   RedirectAttributes redirectAttributes) {

        Boolean hasError = false;
        String message = new String();
        try {
            model.addAttribute("dentists", dentistService.getAll());
            //Add message to model, if an error occurred.
            message = messageSource
                    .getMessage("register.error", null, LocaleContextHolder.getLocale());

            if (bindingResult.hasErrors()) {
                hasError = true;
            }

            //Passed only dentist id from the template.
            visit.dentist = dentistService.getById(visit.getDentistId());
            //Passed date and time strings from JQuery pickers.
            visit.visitDateTime =
                    convertDateTimeStringsToDate(visit.visitDateString, visit.visitTimeString);

            //Check if id path variable is present, to know add or update visit.
            if (id.isPresent()) {
                visit.id = id.get();
            }

            //Checks is this dentist time busy or not.
            if (dentistVisitService.getByDentistIdAndDateTime(visit.getDentistId(), visit.getVisitDateTime()) != null) {
                hasError = true;
                message = messageSource.getMessage("visit.exists.error", null, LocaleContextHolder.getLocale());
            } else {
                dentistVisitService.saveVisit(visit);
            }

        } catch (Exception ex) {
            hasError = true;
        }

        if (hasError) {
            model.addAttribute("message", message);
            return "/form";
        }
        redirectAttributes.addFlashAttribute("isSuccess", true);

        return "redirect:/results";
    }


    private Date convertDateTimeStringsToDate(String date, String time) throws ParseException {

        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String fullDate = date + " " + time;
        Date visitTime = formatter.parse(fullDate);
        return visitTime;

    }

    private String[] convertDateToDateAndTimeStrings(Date date) {
        return date.toString().split("\\s+");
    }
}
