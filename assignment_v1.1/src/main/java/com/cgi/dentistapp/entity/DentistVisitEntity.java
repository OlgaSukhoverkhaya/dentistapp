package com.cgi.dentistapp.entity;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "dentist_visit")
public class DentistVisitEntity extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "dentist_id")
    @NotNull
    private DentistEntity dentist;

    @NotNull
    @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm")
    private Date visitDateTime;

    public Date getVisitDateTime() {
        return visitDateTime;
    }

    public void setVisitDateTime(Date visitDateTime) {
        this.visitDateTime = visitDateTime;
    }


    public DentistEntity getDentist() {
        return dentist;
    }

    public void setDentist(DentistEntity dentist) {
        this.dentist = dentist;
    }
}
