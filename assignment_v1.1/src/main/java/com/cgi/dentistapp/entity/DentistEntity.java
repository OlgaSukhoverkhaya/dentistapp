package com.cgi.dentistapp.entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "dentist")
public class DentistEntity extends BaseEntity {

    @NotBlank
    @Length(min = 1, max = 25)
    private String firstname;

    @NotNull
    @Length(min = 1, max = 25)
    private String lastname;


    @OneToMany(mappedBy = "dentist")
    private List<DentistVisitEntity> visits;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<DentistVisitEntity> getVisits() {
        return visits;
    }

    public void setVisits(List<DentistVisitEntity> visits) {
        this.visits = visits;
    }
}



